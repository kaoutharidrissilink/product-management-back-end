package com.kyanja.productmanagement.controller;


import com.kyanja.productmanagement.dto.ProductDto;
import com.kyanja.productmanagement.dto.StockDto;
import com.kyanja.productmanagement.exception.ResourceNotFoundException;
import com.kyanja.productmanagement.model.Product;
import com.kyanja.productmanagement.model.Stock;
import com.kyanja.productmanagement.service.IProductService;

import com.kyanja.productmanagement.service.IStockService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);


    private final ModelMapper modelMapper;


    private final IProductService productService;


    private final IStockService stockService;


    @Autowired
    public ProductController(ModelMapper modelMapper, IProductService productService, IStockService stockService) {
        this.modelMapper = modelMapper;
        this.productService = productService;
        this.stockService = stockService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<ProductDto> getProductDetailsByListOfSkus(@RequestParam("skus") List<String> skus) throws ResourceNotFoundException {

        return productService.getProductDetailsBySkuList(skus).stream().
                map(product -> modelMapper.map(product, ProductDto.class))
                .collect(Collectors.toList());
    }


    @RequestMapping(value = "/{sku}", method = RequestMethod.GET)
    public ResponseEntity<StockDto> getStockProductDetailsBySku(@PathVariable("sku") String sku) throws ResourceNotFoundException {

        LOGGER.debug("Triggered  stock product details with sku : " + sku);

        Optional<Stock> stock = stockService.getStockProductCountBySku(sku);

        if (!stock.isPresent()) {

            String message = "Stock  with  sku `" + sku + "` not found";

            throw new ResourceNotFoundException(message);

        }

        // convert entity to DTO
        StockDto stockResponse = modelMapper.map(stock, StockDto.class);

        return new ResponseEntity(stockResponse, HttpStatus.OK);

    }


    @RequestMapping(value = "/save-product", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductDto> createProduct(@RequestBody ProductDto productDto) throws ResourceNotFoundException {

        LOGGER.debug("Triggered product creation of : " + productDto.getSku());

        // convert DTO to entity
        Product productRequest = modelMapper.map(productDto, Product.class);

        Product productEntity = productService.addProduct(productRequest);

        // convert entity to DTO
        ProductDto productResponse = modelMapper.map(productEntity, ProductDto.class);

        return new ResponseEntity<>(productResponse, HttpStatus.CREATED);
    }

    @PutMapping("/update-product/{sku}")
    public ResponseEntity<ProductDto> updateProduct(@RequestBody ProductDto productDto, @PathVariable("sku") String sku) throws ResourceNotFoundException {

        LOGGER.debug("Triggered product update of : " + productDto.getSku());

        Product productToUpdate = productService.getProductDetailsBySku(sku);

        if (productToUpdate != null) {


            // convert DTO to entity
            Product productRequest = modelMapper.map(productDto, Product.class);
            productToUpdate.setName(productRequest.getName());
            productToUpdate.setDescription(productRequest.getDescription());
            productToUpdate.setPrice(productRequest.getPrice());

            productService.updateProduct(productToUpdate);
        }

        // convert entity to DTO
        ProductDto productResponse = modelMapper.map(productToUpdate, ProductDto.class);


        return new ResponseEntity(productResponse, HttpStatus.OK);

    }


    @RequestMapping(value = "/product-list", method = RequestMethod.GET)
    public List<ProductDto> getAllProducts() {
        return productService.getAllProducts().stream().map(product -> modelMapper.map(product, ProductDto.class))
                .collect(Collectors.toList());
    }


}
