package com.kyanja.productmanagement;

import com.kyanja.productmanagement.model.Product;
import com.kyanja.productmanagement.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ProductManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductManagementApplication.class, args);
    }

    @Autowired
    private ProductRepository productRepository;


    public void run(String... args) throws Exception {
        Product product = new Product();
        product.setName("product 1");
        product.setDescription("product 1 desc");
        product.setPrice(100.00);
        product.setSku("product 1 sku");


        // save product
        productRepository.save(product);

        Product product2 = new Product();
        product2.setName("product 2");
        product2.setDescription("product 2 desc");
        product2.setPrice(200.00);

        product2.setSku("product 2 sku");


        // save product 2
        productRepository.save(product2);

        List<String> skus = new ArrayList<>();
        skus.add("product 1 sku");
        skus.add("product 2 sku");

        // products based on in condition
        List<Product> productsIn = productRepository.findBySkuIn(skus);
        productsIn.forEach((p) ->
                System.out.println(p));

}
}
