package com.kyanja.productmanagement.repository;

import com.kyanja.productmanagement.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface StockRepository extends JpaRepository<Stock,Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM Stock WHERE sku =?")
    Optional<Stock> getStockProductCountBySku(String sku);
}
