package com.kyanja.productmanagement.repository;

import com.kyanja.productmanagement.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;



public interface ProductRepository extends JpaRepository<Product, Long> {


    Product findBySku(String sku);
    Product findByName(String name);


    /**
     * Return the products based on SQL in clause condition
     * @param skus
     * @return
     */
    List<Product> findBySkuIn(List<String> skus);




}
