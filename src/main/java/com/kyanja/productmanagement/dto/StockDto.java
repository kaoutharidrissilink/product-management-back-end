package com.kyanja.productmanagement.dto;

import java.io.Serializable;

public class StockDto  implements Serializable {


    private static final long serialVersionUID = 2583995854592103746L;


    private String sku;

    private Integer count;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "StockDto{" +
                "sku='" + sku + '\'' +
                ", count=" + count +
                '}';
    }
}
