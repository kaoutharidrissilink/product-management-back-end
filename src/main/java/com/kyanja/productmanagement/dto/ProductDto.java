package com.kyanja.productmanagement.dto;

import java.io.Serializable;

public class ProductDto  implements Serializable {


    private static final long serialVersionUID = 6819586475879531855L;



    private  String name ;

    private String description ;

    private Double price ;

    private String sku;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public ProductDto() {
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "sku='" + sku + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}
