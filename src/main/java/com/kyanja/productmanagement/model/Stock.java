package com.kyanja.productmanagement.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Stock")
public class Stock implements Serializable {


    private static final long serialVersionUID = 3654104584238025661L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stock_id")
    private Long stockId;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(unique=true,nullable = false)
    private String sku;


    @Column(nullable = false)
    private Integer count;



    public Stock() {
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }


    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "stockId=" + stockId +
                ", product=" + product +
                ", sku='" + sku + '\'' +
                ", count=" + count +
                '}';
    }
}
