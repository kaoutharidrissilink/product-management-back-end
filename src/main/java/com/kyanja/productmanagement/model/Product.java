package com.kyanja.productmanagement.model;

import javax.persistence.*;
import java.io.Serializable;



@Entity
public class Product implements Serializable {


    private static final long serialVersionUID = -8601096765326015619L;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "product_id")
    private Long productId;

    @Column(nullable = false)
    private  String name ;

    @Column(nullable = false)
    private String description ;

    @Column(nullable = false)
    private Double price ;

    @Column(unique=true,nullable = false)
    private String sku;

    @OneToOne(mappedBy = "product",fetch = FetchType.LAZY)
    private Stock stock;

    public Product() {
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", sku='" + sku + '\'' +
                ", stock=" + stock +
                '}';
    }
}
