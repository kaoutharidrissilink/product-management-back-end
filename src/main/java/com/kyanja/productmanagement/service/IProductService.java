package com.kyanja.productmanagement.service;

import com.kyanja.productmanagement.exception.ResourceNotFoundException;
import com.kyanja.productmanagement.model.Product;

import java.util.List;
import java.util.Optional;

public interface IProductService {

    public List<Product> getAllProducts();

    public Product getProductDetailsBySku(String sku) throws ResourceNotFoundException;

    public Product addProduct( Product product) throws ResourceNotFoundException;

    public Product updateProduct(Product product) throws ResourceNotFoundException;

    List<Product> getProductDetailsBySkuList(List<String> skus)  throws ResourceNotFoundException;








}
