package com.kyanja.productmanagement.service;

import com.kyanja.productmanagement.model.Stock;

import java.util.Optional;

public interface IStockService {

    Optional<Stock> getStockProductCountBySku(String sku);
}
