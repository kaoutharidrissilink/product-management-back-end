package com.kyanja.productmanagement.service.impl;

import com.kyanja.productmanagement.model.Stock;
import com.kyanja.productmanagement.repository.ProductRepository;
import com.kyanja.productmanagement.repository.StockRepository;
import com.kyanja.productmanagement.service.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
@Transactional
public class StockServiceImpl implements IStockService {

    private final ProductRepository productRepository;

    private final StockRepository stockRepository;

    @Autowired
    public StockServiceImpl(ProductRepository productRepository, StockRepository stockRepository) {
        this.productRepository = productRepository;
        this.stockRepository = stockRepository;
    }

    @Override
    public Optional<Stock> getStockProductCountBySku(String sku) {
        return stockRepository.getStockProductCountBySku(sku);
    }
}
