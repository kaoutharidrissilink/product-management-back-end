
package com.kyanja.productmanagement.service.impl;

import com.kyanja.productmanagement.exception.ResourceNotFoundException;
import com.kyanja.productmanagement.model.Product;
import com.kyanja.productmanagement.repository.ProductRepository;
import com.kyanja.productmanagement.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;



@Service
@Transactional
public class ProductServiceImpl implements IProductService {


    private ProductRepository productRepository;



    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product getProductDetailsBySku(String sku) throws ResourceNotFoundException {

        if (productRepository.findBySku(sku) ==null) {
            throw new ResourceNotFoundException("Product sku: " + sku + " not found");
        }
        return productRepository.findBySku(sku);

    }

    @Override
    public Product addProduct(Product product) throws ResourceNotFoundException {

        if (product.getProductId() == null) {
            Product productToCreate = new Product();
            productToCreate.setName(product.getName());
            productToCreate.setDescription(product.getDescription());
            productToCreate.setPrice(product.getPrice());
            productToCreate.setSku(product.getSku());
            return productRepository.save(productToCreate);
        } else {
            throw new ResourceNotFoundException("Product  with id " +product.getProductId() + " does already exist");
        }

    }

    @Override
    public  Product updateProduct(Product product) throws ResourceNotFoundException{

        boolean isProductExist = productRepository.existsById(product.getProductId());

        if (isProductExist) {
            Product productToUpdate = new Product();
            productToUpdate.setName(product.getName());
            productToUpdate.setDescription(product.getDescription());
            productToUpdate.setPrice(product.getPrice());
            return productRepository.save(productToUpdate);
        } else {
            throw new ResourceNotFoundException("Product  with id " + product.getProductId() + " does not exist");
        }

    }

    @Override
    public List<Product> getProductDetailsBySkuList(List<String> skus) throws ResourceNotFoundException {
        return productRepository.findBySkuIn(skus);
    }

    public ProductServiceImpl() {
    }



}
