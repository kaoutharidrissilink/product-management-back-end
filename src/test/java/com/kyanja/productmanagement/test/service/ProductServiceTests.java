package com.kyanja.productmanagement.test.service;


import com.kyanja.productmanagement.model.Product;
import com.kyanja.productmanagement.repository.ProductRepository;
import com.kyanja.productmanagement.service.IProductService;
import com.kyanja.productmanagement.service.impl.ProductServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(SpringRunner.class)
public class ProductServiceTests {


    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {



        @Bean
        public ProductServiceImpl productService() {
            return new ProductServiceImpl();
        }
    }

    @Autowired
    private IProductService productService;

    @MockBean
    private ProductRepository productRepository;


    @Test
   public void testAddProduct() throws Exception {


        Product product = new Product();
        product.setName("Mobile");
        product.setDescription("Samsung");
        product.setPrice(800.00);

        productService.addProduct(product);
        Product createdProduct = productRepository.findByName("Mobile");

        assertThat(createdProduct.getName()).isEqualTo("Mobile");


    }




}
